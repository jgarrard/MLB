Git global setup
git config --global user.name "Justin"
git config --global user.email "jgarrard@nd.edu"

Create a new repository
git clone git@gitlab.com:jgarrard/MLB.git
cd MLB
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder
cd existing_folder
git init
git remote add origin git@gitlab.com:jgarrard/MLB.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:jgarrard/MLB.git
git push -u origin --all
git push -u origin --tags